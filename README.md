# IMT2282 Operating Systems

Note: book.md is a version of book.pdf with the only purpose of making it easier for students to copy and paste, so

* Read book.pdf
* copy and paste text from book.md when needed (since copying from PDF-files is error prone)
